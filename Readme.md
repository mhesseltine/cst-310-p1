# Project 1: Sierpinski Gasket
----
## Readme
- This software requires an internet browser to run.
- Make sure a browser such as Internet Explorer, Safari, Firefox, Google Chrome, etc. is installed on your machine.
- A WebGL interpreter is also required. Make sure a program such as JetBrains Webstorm is installed.
- Open WebStorm
- After downloading the project folder to a local directory, choose to open the project.
- Open the Shaders folder in the project
- Open either gasket1.html, gasket2.html, gasket3.html, or gasket4.html
- Select which internet browser to run the program in
- Watch as the Sierpinski Gasket is displayed on the browser.